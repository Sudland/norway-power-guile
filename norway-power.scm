(use-modules (curl)
             (json)
             (srfi srfi-9)
             (ice-9 rdelim)
             (ice-9 format)
             (ice-9 binary-ports))


(define (getUser)
  (getenv "USER"))

(define (token)
  (getenv "TOKEN"))

(define (nozone)
  (getenv "NOZONE"))

(define (get-date)
 (strftime "%Y-%m-%d" (localtime (current-time))))

(define cachefile (format #f "/home/~a/.cache/norway-power" (getUser)))
;(define dataUrl (format #f "https://playground-norway-power.ffail.win?zone=NO~a&date=2022-12-17&key=lol" (nozone)))
(define dataUrl (format #f "https://norway-power.ffail.win?zone=NO~a&date=~a&key=~a" (nozone) (get-date) (token)))

(define (get-current-index-time)
 (strftime "%FT%H:00:00%:z" (localtime (current-time))))

(define (get-norway-power)
  (define handle (curl-easy-init))
  (curl-easy-setopt handle 'url dataUrl)
  (curl-easy-perform handle))

(define (write-json data)
  (cond
    ((json-string->scm data)
     (let ((output-port (open-output-file cachefile)))
       (display data output-port)
       (close output-port)))))

(define (get-norway-power-local)
    (catch 'json-invalid
     (lambda ()
        (read-line (open-input-file cachefile)))
     (lambda (key . args)
        get-norway-power)))

(define (fetch-power-data)
  (cond
    ((file-exists? cachefile)
     (get-norway-power-local))
   (else
     (write-json (get-norway-power))
     (get-norway-power-local))))

(define (power-data-object)
 (json-string->scm (fetch-power-data)))

(define (get-nok)
  (assoc-ref (assoc-ref (power-data-object) (get-current-index-time)) "NOK_per_kWh"))

(define (main)
  (cond
    ((get-nok)
     (display (get-nok)))
   (else
     (write-json (get-norway-power))
     (display (get-nok)))))
(main)
